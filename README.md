How to perform CAVS testing
===========================

Add the CAVS vectors to the cavs-suite project under a relevant
subdirect (e.g., gnutls). The CAVS vectors must be put in a subdirectory
which follows the following naming guidelines:

 * CPU/BITS/FEATURE

That is, for example "x86_64/32/aesni". For the tests that no CPU-specific
feature is used the "none" keyword must be set. Under that directory the
desired CAVS tests must be present with the expected names (e.g., AES,
AES_GCM, DRBG800-90A, ...).

Then trigger the running of all depending projects by adding a tag. For
example:
```
$ git tag fips140_run_2016_05_18
```

Wait for the builds to be completed, and after that take the artifacts of
the builds.


How to add a depending project for CAVS testing
===============================================

 1. Create a git repository for the project. 
    Make sure that gitlab uses clone to repositories (Project settings/Builds)
 2. Add cavs-suite as submodule
 3. Ensure that the tests are run on all the subdirs described above
    correctly. That is, the CPU capabilities are faked when required (e.g., AESNI
    or SSSE3 checks), and the proper suite is run for 32-bit or 64-bit options.
 4. Add runners for these gitlab projects. The runners are the systems we
    use for FIPS140-2 testing.
 6. Create .gitlab-ci.yml to automate run. For example:
```
FIPS140-2 run:
  script:
  - git submodule update --init && git submodule update --remote --merge && make check
  tags:
  - x86-64
  artifacts:
    paths:
      - cavs-suite/gnutls/
```
 7. Create a gitlab trigger to allow trigger-run of the CAVS test suite (gitlab: Settings->Triggers)
 8. Add trigger on cavs-suite .gitlab-ci.yml. For example
```
run_fips140_2_tests:
  stage: deploy
  script:
  - "curl -X POST -F token=TOKEN -F ref=master https://gitlab.com/api/v3/projects/1199551/trigger/builds"
  only:
  - tags

```
